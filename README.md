# TimeInterval

This library helps you to create time intervals.

## Installation:

Run the following command to install:

``` python
pip install time_interval
```

## Usage:

Create a 1hour time interval you like this:

``` python
TimeInterval.hour(1)  # Preferred way.
TimeInterval(1, 'h')  # Alternative solution.
```

You can use the following static methods to create time intervals in different intervals:
``` python
TimeInterval.second(1)  # or TimeInterval(1, 's')
TimeInterval.minute(1)  # or TimeInterval(1, 'm')
TimeInterval.hour(1)    # or TimeInterval(1, 'h')
TimeInterval.day(1)     # or TimeInterval(1, 'd')
TimeInterval.week(1)    # or TimeInterval(1, 'w')
TimeInterval.month(1)   # or TimeInterval(1, 'M')
TimeInterval.year(1)    # or TimeInterval(1, 'Y')
TimeInterval.from_str('1m')
```

***Note:** Lower and uppercase character does not matter (e.g 's' and 'S' both represents second),
the only exception is 'm' and 'M'. 'm' represents a minute while 'M' represents a month.*

### Functions:

You can compare TimeIntervals (==, !=, >, >=, <, <=).

You can use arithmetic operators (+, -, *, /).

``` python
str(TimeInterval.second(5))
>>> '5s'
```