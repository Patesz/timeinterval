import time


class TimeInterval:
    valid_intervals = ['s', 'S', 'm', 'h', 'H', 'd', 'D', 'w', 'W', 'M', 'y', 'Y']

    def __init__(self, time: int, interval: str):
        """
        Creates a new TimeInterval object.

        :param time: Integer.

        :param interval: String.

        Valid values for the interval:
            - s/S -> second
            - m -> minute
            - h/H -> hour
            - d/D -> day
            - w/W -> week
            - M -> month
            - y/Y -> year

        Note: If you can use TimeInterval.sec/min/hour/month/week/year staticmethods,
        because they are less error-prone.
        """
        self.time = time

        if interval not in TimeInterval.valid_intervals:
            raise ValueError(f'Valid interval parameters are: {TimeInterval.valid_intervals}. Invalid value: {interval}')

        self.interval = interval

    @staticmethod
    def sec(time: int):
        return TimeInterval(time, 's')

    @staticmethod
    def min(time: int):
        return TimeInterval(time, 'm')

    @staticmethod
    def hour(time: int):
        return TimeInterval(time, 'h')

    @staticmethod
    def day(time: int):
        return TimeInterval(time, 'd')

    @staticmethod
    def week(time: int):
        return TimeInterval(time, 'w')

    @staticmethod
    def month(time: int):
        return TimeInterval(time, 'M')

    @staticmethod
    def year(time: int):
        return TimeInterval(time, 'Y')

    @staticmethod
    def from_str(ti: str):
        return TimeInterval(int(ti[0: len(ti)-1]), ti[-1])

    def subtract_from_now(self, number_of_times=1) -> int:
        now = time.time()
        return int(now) - self.in_seconds() * number_of_times

    def in_milliseconds(self) -> int:
        return self.in_seconds() * 1000

    def in_seconds(self) -> int:
        if self.interval.lower() == 's':
            return self.time
        elif self.interval == 'm':
            return self.time * 60
        elif self.interval.lower() == 'h':
            return self.time * 3600
        elif self.interval.lower() == 'd':
            return self.time * 86400
        elif self.interval.lower() == 'w':
            return self.time * 604800
        elif self.interval == 'M':
            return self.time * 2629800
        elif self.interval.lower() == 'y':
            return self.time * 31557600

    def __add__(self, other):
        if isinstance(other, TimeInterval):
            return self.in_seconds() + other.in_seconds()
        return self.in_seconds() + other

    def __sub__(self, other):
        if isinstance(other, TimeInterval):
            return self.in_seconds() - other.in_seconds()
        return self.in_seconds() - other

    def __mul__(self, other):
        if isinstance(other, TimeInterval):
            return self.in_seconds() * other.in_seconds()
        return self.in_seconds() * other

    def __truediv__(self, other):
        if isinstance(other, TimeInterval):
            return self.in_seconds() / other.in_seconds()
        return self.in_seconds() / other

    def __str__(self):
        return f'{self.time}{self.interval}'

    def filename_str(self) -> str:
        """ If you are here to ask this method existence, here is the explanation why this boilerplate code exists:

        Windows OS does not differentiate lower and upper case characters.
        So 'BTCUSDT_1m.csv' and 'BTCUSDT_1M.csv' are the same files.

        On the other hand, Binance API requests these intervals in a type of format like this: 1m, 1M etc.
        Use the str method to do that.

        :return: e.g.: 1m, 15m, 3d, 1M+, 1w
        """
        if self.interval == 'M':
            return f'{self.time}{self.interval}+'

        return str(self)

    def __eq__(self, other):
        if isinstance(other, TimeInterval):
            return self.in_seconds() == other.in_seconds()
        return False

    def __ne__(self, other):
        return not self == other

    def __lt__(self, other):
        if isinstance(other, TimeInterval):
            return self.in_seconds() < other.in_seconds()
        return False

    def __le__(self, other):
        if isinstance(other, TimeInterval):
            return self.in_seconds() <= other.in_seconds()
        return False

    def __gt__(self, other):
        if isinstance(other, TimeInterval):
            return self.in_seconds() > other.in_seconds()
        return False

    def __ge__(self, other):
        if isinstance(other, TimeInterval):
            return self.in_seconds() >= other.in_seconds()
        return False

    def __hash__(self):
        return hash(self.in_seconds())

    def _has_greater_interval(self, other):
        return TimeInterval.valid_intervals.index(other.interval) > TimeInterval.valid_intervals.index(self.interval)
