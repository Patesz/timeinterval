from setuptools import setup

with open('README.md', 'r') as f:
    long_description = f.read()

setup(
    name='time_interval',
    version='0.1.0',
    description='Python library which helps you create and manage time intervals.',
    long_description=long_description,
    long_description_content_type='text/markdown',
    py_modules=['time_interval'],
    package_dir={'': 'src'},
    url='https://gitlab.com/Patesz/timeinterval',
    author='Ricky',
    author_email='p.ricky.dev@gmail.com',
    license='MIT',
    install_requires=[],
    test_suite='tests'
)
