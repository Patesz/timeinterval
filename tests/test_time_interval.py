import unittest

from time_interval import TimeInterval


class TestTimeInterval(unittest.TestCase):

    def setUp(self):
        self.min5 = TimeInterval(5, 'm')
        self.hour15 = TimeInterval(15, 'H')
        self.min900 = TimeInterval(900, 'm')
        self.day2 = TimeInterval(2, 'd')

    def test_invalid_object_creation(self):
        self.assertRaises(ValueError, TimeInterval, 1, 'a')
        self.assertRaises(ValueError, TimeInterval, 1, 'b')
        self.assertRaises(ValueError, TimeInterval, 1, '1')
        self.assertRaises(ValueError, TimeInterval, 1, 'x')

    def test_in_seconds(self):
        self.assertEqual(self.min5.in_seconds(), 300)
        self.assertEqual(self.min5.in_milliseconds(), 300000)
        self.assertEqual(str(self.min5), '5m')

        self.assertEqual(self.hour15.in_seconds(), 54000)
        self.assertEqual(self.hour15.in_milliseconds(), 54000000)
        self.assertEqual(str(self.hour15), '15h')

    def test_ratio(self):
        self.assertEqual(self.hour15.ratio(self.min5), 180)

    def test_timestamp_ratio(self):
        self.assertEqual(self.min5.timestamp_ratio(600, unit='s'), 2.0)
        self.assertEqual(self.min5.timestamp_ratio(150000, unit='ms'), 0.5)

    def test_equal(self):
        self.assertFalse(self.min5 == 5)
        self.assertFalse(self.min5 == self.hour15)
        self.assertTrue(self.min900 == self.hour15)
        self.assertFalse(self.hour15 == self.day2)

    def test_not_equal(self):
        self.assertTrue(self.min5 != 5)
        self.assertTrue(self.min5 != self.hour15)
        self.assertFalse(self.min900 != self.hour15)
        self.assertTrue(self.hour15 != self.day2)

    def test_less(self):
        self.assertTrue(self.min5 < self.hour15)
        self.assertFalse(self.min900 < self.hour15)
        self.assertTrue(self.min900 < self.day2)

    def test_less_equal(self):
        self.assertTrue(self.min5 <= self.hour15)
        self.assertTrue(self.min900 <= self.hour15)
        self.assertTrue(self.min900 < self.day2)

    def test_greater(self):
        self.assertFalse(self.min5 > self.hour15)
        self.assertFalse(self.min900 > self.hour15)
        self.assertFalse(self.min900 > self.day2)

    def test_greater_equal(self):
        self.assertFalse(self.min5 >= self.hour15)
        self.assertTrue(self.min900 >= self.hour15)
        self.assertFalse(self.min900 >= self.day2)

    def test_hash(self):
        self.assertTrue(hash(self.min900) == hash(self.hour15))
        self.assertFalse(hash(self.min5) == hash(self.hour15))

    def test_from_str(self):
        self.assertEqual(self.min5, TimeInterval.from_str('5m'))
        self.assertEqual(self.hour15, TimeInterval.from_str('15h'))
        self.assertEqual(self.min900, TimeInterval.from_str('900m'))


if __name__ == '__main__':
    unittest.main()
